var fs = require('fs');

var _static = require('node-static');
var file = new _static.Server('./static', {
    cache: false
});

var app = require('http').createServer(serverCallback);

function serverCallback(request, response) {
    request.addListener('end', function () {
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        response.setHeader('Access-Control-Allow-Headers', 'Content-Type');

        file.serve(request, response);
    }).resume();
}


var io = require('socket.io').listen(app, {
    log: false,
    origins: '*:*'
});

io.set('transports', [
     'websocket',
    'xhr-polling',
    'jsonp-polling'
]);

//io.set('heartbeat interval', 2000);
//io.set('heartbeat timeout', 5000);

//var channels = {}; //channels[username] = sd;
var online = {}; //online[user.id] = {id: socket.id, busy: boolen};

io.sockets.on('connection', function (socket) {
    //var initiatorChannel = '';
    if (!io.isConnected) {
        io.isConnected = true;
    }
    socket.on('whoOnLine', function(callback){
        callback(JSON.stringify(online)); // TODO: transfer only username and busy-status
    });

    socket.on('online', function(user){
        online[user.id] = {id: socket.id, busy: false, name: user.name, surname: user.surname};
        socket.broadcast.emit('userOnline', user);
    });

    socket.on('call', function(data, callback){
        console.log(JSON.stringify(data));

        var callerId = data.caller,
            callee = online[data.callee],
            caller = online[callerId];

        if (callee.busy || caller.busy){ //callee or caller are speaking already
            callback(false);//it's not multiple chat allowed
        }
        else{
            caller.busy = true;
            caller = {
                id: callerId,
                name: caller.name,
                surname: caller.surname
            };
            io.sockets.socket(callee.id).emit('incomingCall', caller, data.sd, function(res){
                if(res){
                    //channels[data.caller] = data.sd;
                    callee.busy = true;
                    io.sockets.emit('userBusy', [data.callee, callerId]);
                }
                else{
                    caller.busy = false;
                }
                callback(res);
            })
        }
    });

    socket.on('userIdle', function(uid){
        //delete channels[username];
        online[uid]['busy'] = false;
        socket.broadcast.emit('userIdle', uid);
    });

/*    socket.on('set_sd', function(data, callback){
        channels[data.username] = data.sd;
        callback();
    });

    socket.on('get_sd', function(username, callback){
        for (var channel in channels){
            if (channels.hasOwnProperty(channel)){
                if (channel === username){
                    callback(channels[channel]);
                }
            }
        }
    });*/

    socket.on('disconnect', function (channel) {

        for (var r in online){
            if (online.hasOwnProperty(r)){
                if (online[r]['id'] === socket.id){
                    delete online[r];//TODO: stop iteration
                    //delete channels[r];
                    socket.broadcast.emit('userOffline', r);
                }
            }
        }

        /*if (initiatorChannel) {
            delete channels[initiatorChannel];
        }*/
    });
});

/*function onNewNamespace(channel, sender) {
    io.of('/' + channel).on('connection', function (socket) {
        console.log('namespace connected: ' + channel);
        var username;
        if (io.isConnected) {
            io.isConnected = false;
            socket.emit('connect', true);
        }

        socket.on('message', function (data) {
            if (data.sender == sender) {
                if(!username) username = data.data.sender;

                socket.broadcast.emit('message', data.data);
            }
        });

        socket.on('disconnect', function() {
            console.log('user disconnected namespace');
            if(username) {
                socket.broadcast.emit('user-left', username);
                username = null;
            }
        });
    });
}*/


app.listen(process.env.PORT);
